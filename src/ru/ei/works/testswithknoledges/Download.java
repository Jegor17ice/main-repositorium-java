package ru.ei.works.testswithknoledges;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Download {
    private String url;
    private String outputWay;


    public Download(String url, String outputWay) {

        this.url = url;

        this.outputWay = outputWay;
    }

    public void download() {
        try (InputStream in = new URL(url).openStream()) {
            Files.copy(in, Paths.get(outputWay));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
