package ru.ei.works.lossofresult;

/**
 * Класс для инкрементирования общей переменной
 */

public class InterferenceThread extends Thread {
    private final InterferenceExample checker;
    private static int i;

    InterferenceThread(InterferenceExample checker) {
        this.checker = checker;
    }

    private static synchronized void increment() {
        i++;
    }

    int getI() {
        return i;
    }

    public void run() {
        while (!checker.stop()) {
            increment();
        }
    }
}