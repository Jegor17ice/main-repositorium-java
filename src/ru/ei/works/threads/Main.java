package ru.ei.works.threads;

/**
 *прогрмма реализует создние и запуск 10 потоков
 * @author Иванчин Егор 17ит18
 */
public class Main extends Thread{

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {

            (new Main()).start();

        }
        System.out.println("Глвный поток запущен");

    }
    @Override
    public void run() {
        System.out.println("Hello from a thread! "+ getName());
    }
}