package ru.ei.works.bankaccount;

/**
 * Класс Account содержит поле balance для размещения баланса банковского счета, а также методы для пополнения баланса и снятия денег со счета.
 */
public class Account {

    private long balance;

    /**
     * Конструктор класса без параметоров
     */
    public Account() {
        this(0);
    }

    /**
     * Конструктор класса с параметорами
     * @param balance - баланс
     */
    public Account(long balance) {
        this.balance = balance;
    }

    /**
     * Метод пополнения бланса
     *
     * @param money - сумма пополнения баланса
     * @throws NegativeException
     */
    public synchronized void deposit(long money) throws NegativeException {
        if (!isPositive(money)) {
            throw new NegativeException();
        }
        balance += money;

    }

    /**
     * Метод снятия аланса
     *
     * @param money - сумма снятия с баланса
     * @throws OutOfBalanceException
     * @throws NegativeException
     */
    public synchronized void withdraw(long money) throws OutOfBalanceException, NegativeException {
        if (!isPositive(money)) {
            throw new NegativeException();
        }
        if (money > balance) {
            throw new OutOfBalanceException();
        }
        balance -= money;
    }
    /**
     * Метод проверки баланса
     * @return - состояние баланса
     */
    public synchronized long getBalance() {
        return balance;
    }

    /**
     * Метод проверки входных данных на положительность
     * @param numb - проверяемые данные
     * @return - результат проверки
     */
    private boolean isPositive(long numb) {
        return numb >= 0;
    }

}
