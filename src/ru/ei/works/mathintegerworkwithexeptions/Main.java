package ru.ei.works.mathintegerworkwithexeptions;

import java.util.*;

/**
 * класс реализующий работу над вводом
 * @author Иванчин Егор 17ит18
 */
public class Main {

    public static void main(String args[]) {

        System.out.println("Введите пример вида   ПЕРВЫЙ ОПЕРАНД_ОПЕРАЦИЯ_ВТОРОЙ ОПЕРАНД");
        String[] scStrings = new String[3];
        try {
            scStrings = scan();
        } catch (NumberFormatException e) {

        }
        if (scStrings[0] == null) {
            System.out.println("Введено неверно!");
        } else {
            int arr[] = parse(scStrings);
            try {
                System.out.println(MathInteger.doOperation(arr[0], arr[1], scStrings[1]));
            }catch (Exception e){
                System.out.println("Что-то пошло не так");
            }
        }
    }

    /**
     * выделение из массива - чисел
     *
     * @param scStrings стринговый массив изначльной строки
     * @return массив с двумя элементами
     */
    public static int[] parse(String scStrings[]) {
        int arr[] = new int[2];
        arr[0] = Integer.parseInt(scStrings[0]);
        arr[1] = Integer.parseInt(scStrings[2]);
        return arr;
    }

    /**
     * реализующет ввод выражения с консоли
     *
     * @return возврщает массив строк выражения
     * @throws NumberFormatException
     */
    public static String[] scan() throws NumberFormatException {
        Scanner sc = new Scanner(System.in);
        String scString = sc.nextLine();
        if (scString.matches("[-+]?[0-9]+\\s[/*-+^%]{1}\\s[-+]?[0-9]+")) {
            String[] scStrings = scString.split(" ");
            return scStrings;
        } else {
            throw new NumberFormatException();
        }

    }

}