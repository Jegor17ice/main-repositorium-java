package ru.ei.works.mathintegerworkwithexeptions;

/**
 * Класс реализует математические операции
 */
public class MathInteger {
    /**
     * Реализует операцию сложения первого и второго операнда
     *
     * @param a первый операнд
     * @param b второй операнд
     * @return сумму a и b
     * @throws ArithmeticException переполнение
     */
    public static int add(int a, int b) throws IntOverflowException {
        int r = a + b;
        if (((a ^ r) & (b ^ r)) < 0) {
            throw new IntOverflowException();
        }
        return r;
    }

    /**
     * Реализует операцию разностьи первого и второго операнда
     *
     * @param a первый операнд
     * @param b второй операнд
     * @return разность a и b
     * @throws ArithmeticException переполнение
     */
    public static int sub(int a, int b) throws IntOverflowException {
        int r = a - b;
        if (((a ^ b) & (a ^ r)) < 0) {
            throw new IntOverflowException();
        }
        return r;
    }

    /**
     * Реализует операцию деления первого операнда на второй
     *
     * @param a первый операнд
     * @param b второй операнд
     * @return результат деления a на b
     * @throws ArithmeticException деление на ноль
     */
    public static int div(int a, int b) throws NullExeption {
        if (b == 0) {
            throw new NullExeption();
        }

        return a / b;
    }

    /**
     * Реализует операцию произведения первого операнда на второй
     *
     * @param a первый операнд
     * @param b второй операнд
     * @return произведение a и b
     * @throws ArithmeticException переполнение
     */
    public static int multiply(int a, int b) throws IntOverflowException {
        long r = (long) a * (long) b;
        if ((int) r != r) {
            throw new IntOverflowException();
        }

        return (int) r;
    }

    /**
     * Реализует нахождния остатка от деления
     *
     * @param a первый операнд
     * @param b второй опернад
     * @return остаток от деления
     * @throws NullExeption ошибка деления на ноль
     */
    public static int divOst(int a, int b) throws NullExeption {
        if (b == 0) {
            throw new NullExeption();
        }

        return a % b;
    }

    /**
     * Реализует операцию возведения первого операнда в степень второго операнда
     *
     * @param a первый операнд
     * @param b второй операнд
     * @return a в степени b
     * @throws ArithmeticException переполнение
     */
    public static int pow(int a, int b) throws IntOverflowException {
        long c = 1;
        for (int i = 0; i < b; i++) {
            c = c * a;
        }
        c = (long) a * (long) b;
        if ((int) c != c) {
            throw new IntOverflowException();
        }
        return (int) c;
    }

    /**
     * определяет операцию по значению знака и возвращает результат операции
     *
     * @param a         значение 1 операнда
     * @param b         значение 2 операнда
     * @param operation знак операции
     * @return результат операции
     */
    public static int doOperation(int a, int b, String operation) {
        int result = 0;
        switch (operation) {
            case "*": {
                try {
                    result = MathInteger.multiply(a, b);
                } catch (IntOverflowException e) {
                    throw new IntOverflowException();
                }
                break;
            }
            case "/": {
                try {
                    result = MathInteger.div(a, b);
                } catch (NullExeption e) {
                    throw new NullExeption();
                }
                break;
            }
            case "-": {
                try {
                    result = MathInteger.sub(a, b);
                } catch (IntOverflowException e) {
                    throw new IntOverflowException();
                }
                break;
            }
            case "+": {
                try {
                    result = MathInteger.add(a, b);
                } catch (IntOverflowException e) {
                    throw new IntOverflowException();
                }
                break;
            }
            case "^": {
                try {
                    result = MathInteger.pow(a, b);
                } catch (IntOverflowException e) {
                    throw new IntOverflowException();
                }
                break;
            }
            case "%": {
                try {
                    result = MathInteger.divOst(a, b);
                } catch (NullExeption e) {
                    throw new NullExeption();
                }
                break;
            }
        }

        return result;
    }
}