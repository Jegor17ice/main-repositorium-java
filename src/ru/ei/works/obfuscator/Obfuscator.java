package ru.ei.works.obfuscator;

import java.util.*;


public class Obfuscator {
    public static final String REGEX_VAR = "[A-Za-z0-9_]*";
    public static final String REGEX_DATA_TYPE = "(?:String|int|long|float|char|boolean|double|byte|short" +
            "|String\\[]|int\\[]" +
            "|InputStream|OutputStream|NewThread|Thread|Object)";


    public static void obfuskator(String text) {

        ArrayList vocabularyList = new ArrayList();
        vocabularyList = makeVocabulary(text);//создаем словарь
        ptintVocabulary(vocabularyList);//выводим словарь
        String textCopy = text.replaceAll("[\"\\\\\\/\\*\\+\\.\\,\\(\\)\\&\\?\\%\\$\\#\\@\\!\\;\\№\\{\\}\"]+", " ");
        String[] splitLine = textCopy.split("\\s");

        //реплейсим
        String local = null;
        String textFinal = null;
        for (int i = 1; i < splitLine.length; i++) {
                for (int j = 0; j < vocabularyList.size(); j++) {
                  local =  (String) vocabularyList.get(j);
                    //System.out.println("splitLine["+i+"] = " + splitLine[i]);
                if (splitLine[i].matches(local)){
                    System.out.print(splitLine[i]);//вывод всех найденных переменных
                    //textFinal = textFinal + " "+splitLine[i].replaceAll(local,"буква");//кароч проблема в том что local не считает за регулярку
                }
            }
        }

        System.out.println(textFinal);

    }
    public static ArrayList makeVocabulary(String text) {
        String textCopy = text.replaceAll("[\"\\\\\\/\\*\\+\\.\\,\\(\\)\\&\\?\\%\\$\\#\\@\\!\\;\\№\\{\\}\"]+", " ");
        String[] splitLine = textCopy.split("\\s");
        ArrayList vocabularyList = new ArrayList();
        int i, j;
        for (i = 0, j = 0; i < splitLine.length; i++) {
            if (splitLine[i].matches(REGEX_DATA_TYPE)) {
                vocabularyList.add(splitLine[i + 1]);
                j++;
            }
        }

        return vocabularyList;
    }
    public static void ptintVocabulary(ArrayList vocabularyList){
        System.out.println("Словарь:");
        for (int k = 0; k < vocabularyList.size(); k++) {
            System.out.println("Элемент "+k+" = "+ vocabularyList.get(k));
        }
    }
}
