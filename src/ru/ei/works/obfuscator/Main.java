package ru.ei.works.obfuscator;

public class Main {
    public static void main(String[] args) {
        String text = "public class Main extends Thread{\n" +
                "    public static void main(String[] args) throws IOException, InterruptedException {\n" +
                "\n" +
                "        long parallelCopyTimeBefore = System.currentTimeMillis();\n" +
                "\n" +
                "        NewThread copyThread1 = new NewThread(inputWay1,outputWay1);\n" +
                "        NewThread copyThread2 = new NewThread(inputWay2,outputWay2);\n" +
                "\n" +
                "        copyThread1.thread.start();\n" +
                "        copyThread2.thread.start();\n" +
                "\n" +
                "        copyThread1.thread.join();\n" +
                "        copyThread2.thread.join();\n" +
                "\n" +
                "        long parallelCopyTimeAfter = System.currentTimeMillis();\n" +
                "\n" +
                "        consoleTime(parallelCopyTimeBefore, parallelCopyTimeAfter);\n" +
                "\n" +
                "        long sequenceCopyTimeBefore = System.currentTimeMillis();\n" +
                "\n" +
                "        CopyFile cf = new CopyFile(inputWay1, outputWay1);\n" +
                "        cf.copy();\n" +
                "        CopyFile cf2 = new CopyFile(inputWay2, outputWay2);\n" +
                "        cf2.copy();\n" +
                "\n" +
                "        long sequenceCopyTimeAfter = System.currentTimeMillis();\n" +
                "\n" +
                "        consoleTime(sequenceCopyTimeBefore, sequenceCopyTimeAfter);\n" +
                "\n" +
                "        NewThreadWithNIO copyNIOThread1 = new NewThreadWithNIO(inputWay1, outputWay1);\n" +
                "        NewThreadWithNIO copyNIOThread2 = new NewThreadWithNIO(inputWay1, outputWay1);\n" +
                "\n" +
                "        copyNIOThread1.thread.start();\n" +
                "        copyNIOThread2.thread.start();\n" +
                "\n" +
                "        copyNIOThread1.thread.join();\n" +
                "        copyNIOThread2.thread.join();\n" +
                "\n" +
                "\n" +
                "        long sequenceCopyTimeBeforeWithNIO = System.currentTimeMillis();\n" +
                "\n" +
                "        CopyFile c = new CopyFile(inputWay1, outputWay1);\n" +
                "        c.copy();\n" +
                "        CopyFile c2 = new CopyFile(inputWay2, outputWay2);\n" +
                "        c2.copy();\n" +
                "\n" +
                "        long sequenceCopyTimeAfterWithNIO = System.currentTimeMillis();\n" +
                "\n" +
                "        consoleTime(sequenceCopyTimeBeforeWithNIO, sequenceCopyTimeAfterWithNIO);\n" +
                "    }\n" +
                "\n" +
                "    public static void consoleTime(long before, long after){\n" +
                "        System.out.println(\"copy \"+(after - before)+\" ms\");\n" +
                "\n" +
                "    }";
        Obfuscator.obfuskator(text);
    }
}
