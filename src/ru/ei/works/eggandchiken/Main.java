package ru.ei.works.eggandchiken;

/**
 * Программа для разрешить спор: "Что появилось сначала - яйцо или курица?"
 *
 * @author Иванчин Егор 17ит18
 */
public class Main {

    public static void main(String[] args){

        Evolution egg = (new Evolution("яйцо"));
        Evolution chicken = (new Evolution("курица"));

        try {
            egg.thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.print("Во вселенной первым появилось");
        if (chicken.thread.isAlive()){
            System.out.print("Курицо");
        }else{
            System.out.print(" Яйцо");
        }
    }

}
