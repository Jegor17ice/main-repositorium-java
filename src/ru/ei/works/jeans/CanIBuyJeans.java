package ru.ei.works.jeans;

public class CanIBuyJeans {

    public static String Think(int price, int discond, int cash){
        String out;
        if ((cash<0)||(price<0)||(discond<0)){
            out = "введено отрицательное значение";
        }else {
            if (price - ((price / 100) * discond) <= cash) {
                out = "денег хватит";
            } else {
                out = "денег не хватит";
            }
        }
        System.out.println(out);
        return out;
    }
}
