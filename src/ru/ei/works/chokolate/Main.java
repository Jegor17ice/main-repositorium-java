package ru.ei.works.chokolate;

/**
 * Класс реализует подсчёт шоколадок
 */
class Chokolate {
    /**
     * Функция вычисления количества шоколадок
     *
     * @param money - количество денег
     * @param price - цена одной шоколадки
     * @param wrap  - количество обёрток, для получения одной шоколадки
     * @return возвращает количество полученных шоколадок или ошибку отрицательного результата(-111)
     */
    public static int chocolateCount(int money, int price, int wrap) {
        if ((money < 0) || (price < 0) || (wrap < 0)) {
            return -111;
        } else {
            return (money / price) + (money / price) / wrap;
        }
    }

}