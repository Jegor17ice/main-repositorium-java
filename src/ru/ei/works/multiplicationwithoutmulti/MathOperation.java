package ru.ei.works.multiplicationwithoutmulti;

/**
 * Класс вычисления произведения
 */
class MathOperation {
    /**
     * Функция вычисления произведения
     *
     * @param firstOperand  - первый элемент произведения
     * @param secondOperand - второй элемент произведения
     * @return возвращает результат произвдения и first_operand second_operand
     */
    public static int multiply(int firstOperand, int secondOperand) {

        int loopsCount = 0;
        int numbPlus = 0;
        boolean numbIsInvert = false;
        if(firstOperand==0||secondOperand==0){
            return 0;
        }
        if(((firstOperand<0)&&(secondOperand>0))||
                ((secondOperand<0)&&(firstOperand>0))){
            numbIsInvert = true;
        }
        if(firstOperand > secondOperand){
            loopsCount = Math.abs(secondOperand);
            numbPlus = Math.abs(firstOperand);
        }else {
            loopsCount = Math.abs(firstOperand);
            numbPlus = Math.abs(secondOperand);
        }
        int resultMultiplication = 0;
        for(int i = 0;i < loopsCount;i++){
            resultMultiplication += numbPlus;
        }
        if(numbIsInvert){
            resultMultiplication=-resultMultiplication;
        }
        return resultMultiplication;
    }
}
