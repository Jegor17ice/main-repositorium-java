package ru.ei.works.sortingbyfreqency;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Тесты для класса сортировки
 */
public class SortTest {
    /**
     * Сортировка массива из 9 элементов
     */
    @Test
    public void sortSizeNine() {
        int[] arr = {0, 2, 1, 1, 0, 2, 2, 1};
        int[] arr2 = {2, 2, 2, 1, 1, 1, 0, 0};
        assertArrayEquals(arr2, Sort.sort(arr));
    }
    /**
     * Сортировка массива из 30 элементов
     */
    @Test
    public void sortSizeTherty() {
        int[] arr = {0, 4, 6, 22, 2, 6, 3, 6, 7, 8, 2, 7, 8, 4, 6, 7, 3, 7, 8, 8, 2, 5, 6, 7};
        int[] arr2 = {6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 8, 8, 8, 8, 2, 2, 2, 4, 4, 3, 3, 0, 22, 5};
        assertArrayEquals(arr2,Sort.sort(arr));
    }
    /**
     * Сортировка массива из отрицательных и положительных элементов
     */
    @Test
    public void sortNegativeAndPositive() {
        int[] arr = {0, 2, 1, -1, 0, 2, 2, 1, -4, -1};
        int[] arr2 = {2, 2, 2, 0, 0, 1, 1, -1, -1, -4};
        assertArrayEquals(arr2, Sort.sort(arr));
    }
    /**
     * Сортировка массива из одного элемента
     */
    @Test
    public void sortSizeOne() {
        int[] arr = {1};
        int[] arr2 = {1};
        assertArrayEquals(arr2, Sort.sort(arr));
    }
    /**
     * Сортировка массива из нулей
     */
    @Test
    public void sortOnlyZero() {
        int[] arr = {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        int[] arr2 = {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        assertArrayEquals(arr2, Sort.sort(arr));
    }
    /**
     * Сортировка массива из отрицательных элементов
     */
    @Test
    public void sortNegative() {
        int[] arr = {-3,-8,-22,-4,-4,-3,-6,-8,-22,-1};
        int[] arr2 = {-3, -3, -8, -8, -22, -22, -4, -4, -6, -1};
        assertArrayEquals(arr2, Sort.sort(arr));
    }
    /**
     * Сортировка пустого массива
     */
    @Test
    public void sortEmpty() {
        int[] arr = {};
        int[] arr2 = {};
        assertArrayEquals(arr2, Sort.sort(arr));
    }
    /**
     * Сортировка массива 10 единиц и одной тройки
     */
    @Test
    public void sortTenToOne() {
        int[] arr = {1,1,1,1,3,1,1,1,1};
        int[] arr2 = {1,1,1,1,1,1,1,1,3};
        assertArrayEquals(arr2, Sort.sort(arr));
    }
}