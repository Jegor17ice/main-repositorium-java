package ru.ei.works.multiplicationwithoutmulti;

import org.junit.Test;

import static org.junit.Assert.*;

public class MathOperationTest {

    @Test
    public void multiplyPositive() {
        int a = 100 + (int) (Math.random() * 200);
        int b = 100 + (int) (Math.random() * 100);

        assertEquals(49, MathOperation.multiply(7, 7));
        assertEquals(a * b, MathOperation.multiply(a, b));
    }

    @Test
    public void multiplyZeroByZero() {
        int a = 0;
        int b = 0;

        assertEquals(0, MathOperation.multiply(a, b));
    }

    @Test
    public void multiplyNegative() {
        int a = -100 + (int) (Math.random() * 100);
        int b = -100 + (int) (Math.random() * 100);

        assertEquals(49, MathOperation.multiply(-7, -7));
        assertEquals(a * b, MathOperation.multiply(a, b));
    }
    @Test
    public void multiplyNegativeAndPositive() {
        int a = -100 + (int) (Math.random() * 100);
        int b = 100 + (int) (Math.random() * 100);

        assertEquals(-49, MathOperation.multiply(-7, 7));
        assertEquals(a * b, MathOperation.multiply(a, b));
    }
    @Test
    public void multiplyZeroByNum() {
        int a = 0;
        int b = -100 + (int) (Math.random() * 200);
        assertEquals(0, MathOperation.multiply(7, 0));
        assertEquals(0, MathOperation.multiply(a, b));
    }
    @Test
    public void multiplyNumByZero() {
        int a = -100 + (int) (Math.random() * 200);
        int b = 0;
        assertEquals(0, MathOperation.multiply(7, 0));
        assertEquals(0, MathOperation.multiply(a, b));
    }
}