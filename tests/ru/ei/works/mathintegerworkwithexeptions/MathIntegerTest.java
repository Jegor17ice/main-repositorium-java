package ru.ei.works.mathintegerworkwithexeptions;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Тесты для консольного калькулятора
 */
public class MathIntegerTest {
    /**
     * операция сложения двух положительных чисел
     */
    @Test
    public void add() {
        int a = 3;
        int b = 4;
        assertEquals(a + b, MathInteger.add(a, b));
    }
    /**
     * операция сложения положительного числа и нуля
     */
    @Test
    public void addZero() {
        int a = 3;
        int b = 0;
        assertEquals(a, MathInteger.add(a, b));
    }
    /**
     * операция сложения положительного числа и отрицательного
     */
    @Test
    public void addNegative() {
        int a = 3;
        int b = -3;
        assertEquals(a + b, MathInteger.add(a, b));
    }
    /**
     * операция сложения двух отрицательных чисел
     */
    @Test
    public void addOnlyNegative() {
        int a = -4;
        int b = -5;
        assertEquals(a + b, MathInteger.add(a, b));
    }
    /**
     * операция вычитания двух положительных чисел
     */
    @Test
    public void sub() {
        int a = 3;
        int b = 4;
        assertEquals(a - b, MathInteger.sub(a, b));
    }
    /**
     * операция вычитания положительного числа и отрицательного
     */
    @Test
    public void subNegative() {
        int a = 3;
        int b = -4;
        assertEquals(a - b, MathInteger.sub(a, b));
    }
    /**
     * операция вычитания положительного числа и нуля
     */
    @Test
    public void subZero() {
        int a = 3;
        int b = 0;
        assertEquals(a - b, MathInteger.sub(a, b));
    }
    /**
     * операция сложения двух отрицательных чисел
     */
    @Test
    public void subOnlyNegative() {
        int a = -3;
        int b = -4;
        assertEquals(a - b, MathInteger.sub(a, b));
    }
    /**
     * операция деления двух положительных чисел
     */
    @Test
    public void div() {
        int a = 8;
        int b = 2;
        assertEquals(a / b, MathInteger.div(a, b));
    }
    /**
     * операция деления отрицательного и положительного числа
     */
    @Test
    public void divNegative() {
        int a = 8;
        int b = -2;
        assertEquals(a / b, MathInteger.div(a, b));
    }

    /**
     * операция деления двух отрицательных чисел
     */
    @Test
    public void divOnlyNegative() {
        int a = -8;
        int b = -2;
        assertEquals(a / b, MathInteger.div(a, b));
    }
    /**
     * операция умножения двух положительных чисел
     */
    @Test
    public void multiply() {
        int a = 4;
        int b = 3;
        assertEquals(a * b, MathInteger.multiply(a, b));
    }
    /**
     * операция умножения числа на ноль
     */
    @Test
    public void multiplyZero() {
        int a = 4;
        int b = 0;
        assertEquals(a * b, MathInteger.multiply(a, b));
    }
    /**
     * операция умножения числа на отрицательное
     */
    @Test
    public void multiplyNegative() {
        int a = -4;
        int b = 3;
        assertEquals(a * b, MathInteger.multiply(a, b));
    }
    /**
     * операция умножения двух отрицательных чисел
     */
    @Test
    public void multiplyOnlyNegative() {
        int a = -4;
        int b = -3;
        assertEquals(a * b, MathInteger.multiply(a, b));
    }
    /**
     * операция деления с остатком двух положительных чисел
     */
    @Test
    public void divOst() {
        int a = 7;
        int b = 2;
        assertEquals(a % b, MathInteger.divOst(a, b));
    }

    /**
     * операция деления с остатком на отрицтельное число
     */
    @Test
    public void divOstNegative() {
        int a = 7;
        int b = -2;
        assertEquals(a % b, MathInteger.divOst(a, b));
    }
    /**
     * операция деления с остатком двух отрицательных чисел
     */
    @Test
    public void divOstOnlyNegative() {
        int a = -7;
        int b = -2;
        assertEquals(a % b, MathInteger.divOst(a, b));
    }
    /**
     * операция возведения в степень
     */
    @Test
    public void pow() {
        int a = 4;
        int b = 2;
        assertEquals(16, MathInteger.pow(a, b));
    }
    /**
     * операция возведения в степень нуля
     */
    @Test
    public void powZero() {
        int a = 4;
        int b = 0;
        assertEquals(1, MathInteger.pow(a, b));
    }

    /**
     * операция возведения в степень отрицательного числа
     */
    @Test
    public void powNegative() {
        int a = -4;
        int b = 2;
        assertEquals(16, MathInteger.pow(a, b));
    }
    /**
     * операция возведения в степень двух отрицтельных чисел
     */
    @Test
    public void powOnlynegative() {
        int a = -4;
        int b = -2;
        assertEquals(16, MathInteger.pow(a, b));
    }
}