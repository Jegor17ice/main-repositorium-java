package ru.ei.works.jeans;

import org.junit.Test;

import static org.junit.Assert.*;

public class CanIBuyJeansTest {

    @Test
    public void CanBuy() {
        int price = 3400;
        int discond = 50;
        int cash = 1700;
        assertEquals("денег хватит", CanIBuyJeans.Think(price,discond,cash));
    }

    @Test
    public void CanNotBuy() {
        int price = 3400;
        int discond = 50;
        int cash = 1500;
        assertEquals("денег не хватит", CanIBuyJeans.Think(price,discond,cash));
    }

    @Test
    public void NegativValue() {
        int price = -3400;
        int discond = 50;
        int cash = 1500;
        assertEquals("введено отрицательное значение", CanIBuyJeans.Think(price,discond,cash));
    }
}