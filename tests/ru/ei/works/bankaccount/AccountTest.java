package ru.ei.works.bankaccount;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Тесты для класса Account
 */
public class AccountTest {
    /**
     * Тест пополнения баланса на 0
     * @throws NegativeException
     */
    @Test
    public void depositZero() throws NegativeException {
        Account account = new Account(100);
        account.deposit(0);
        long expected = account.getBalance();
        long actual = 100;
        assertEquals(expected, actual);
    }
    /**
     * Тест пополнения баланса на положительное значение
     * @throws NegativeException
     */
    @Test
    public void depositPositive() throws NegativeException {
        Account account = new Account(100);
        account.deposit(10);
        long expected = account.getBalance();
        long actual = 110;
        assertEquals(expected, actual);
    }
    /**
     * Тест пополнения баланса на отрицательное значение
     * @throws NegativeException
     */
    @Test(expected = NegativeException.class)
    public void depositNegativeException() throws NegativeException {
        Account account = new Account(100);
        account.deposit(-7);

    }
    /**
     * Тест снятия с баланса 0
     * @throws NegativeException
     */
    @Test
    public void withdrawZero() throws NegativeException, OutOfBalanceException {
        Account account = new Account(100);
        account.withdraw(0);
        long expected = account.getBalance();
        long actual = 100;
        assertEquals(expected, actual);
    }
    /**
     * Тест снятия с баланса положительного знчения
     * @throws NegativeException
     */
    @Test
    public void withdrawPositive() throws NegativeException, OutOfBalanceException {
        Account account = new Account(100);
        account.withdraw(10);
        long expected = account.getBalance();
        long actual = 90;
        assertEquals(expected, actual);
    }
    /**
     * Тест снятия с баланса отрицательного знчения
     * @throws NegativeException
     */
    @Test(expected = NegativeException.class)
    public void withdrawNegativeException() throws NegativeException, OutOfBalanceException {
        Account account = new Account(100);
        account.withdraw(-7);
    }
    /**
     * Тест снятия с баланса слишком большого значения
     * @throws NegativeException
     */
    @Test(expected = OutOfBalanceException.class)
    public void withdrawOutOfBalanceException() throws OutOfBalanceException, NegativeException {
        Account account = new Account(100);
        account.withdraw(200);
    }

}